<h1 align="center">Hi 👋, I'm Constantin Müller</h1>
<img src="https://gitlab.com/0x1C1B/0x1C1B/-/raw/main/images/banner.svg" alt="Banner" width="100%" height="auto"/>

Who am I? I'm a student from Germany, who is interested in computer and data science. For me, programming isn't just a hobby but rather a passion. Hence, I'm a self-taught software developer and data scientist. At the moment I'm studying business informatics with a major in data science in Karlsruhe, Germany. Any further questions? Feel free to ask!¹

This is my private GitLab account, which I mainly use for working in working groups and in projects as part of my studies. For insights into my private work/portfolio or my involvement in open source projects, I refer to my [GitHub profile](https://github.com/0x1C1B). On this platform, I mainly work with other students in the context of hackathons or in a start-up context.

<small>¹I primarily use my [GitHub account](https://github.com/0x1C1B) for project participation. Therefore, the personal information about me in this profile may be out of date. I ask for understanding.</small>

## Skills
These are some of the programming languages I know, frameworks and programming libraries that I know and use. I have taught myself many of the tools listed below over time. I also learned some programming languages and libraries during my academic career.

![](https://img.shields.io/badge/Java-red?logo=openjdk&logoColor=white&style=for-the-badge)
![](https://img.shields.io/badge/Spring%20Boot-green?logo=spring-boot&logoColor=white&style=for-the-badge)
![](https://img.shields.io/badge/JavaScript-yellow?logo=javascript&logoColor=white&style=for-the-badge)
![](https://img.shields.io/badge/React.js-blue?logo=react&logoColor=white&style=for-the-badge)
![](https://img.shields.io/badge/Express.js-gray?logo=express&logoColor=white&style=for-the-badge)
![](https://img.shields.io/badge/Node.js-green?logo=node.js&logoColor=white&style=for-the-badge)
![](https://img.shields.io/badge/Tailwind.css-blue?logo=tailwindcss&logoColor=white&style=for-the-badge)
![](https://img.shields.io/badge/Scikit%20Learn-orange?logo=scikitlearn&logoColor=white&style=for-the-badge)
![](https://img.shields.io/badge/Bootstrap.css-blueviolet?logo=bootstrap&logoColor=white&style=for-the-badge)
![](https://img.shields.io/badge/Python-blue?logo=python&logoColor=white&style=for-the-badge)
![](https://img.shields.io/badge/Gatsby.js-blueviolet?logo=gatsby&logoColor=white&style=for-the-badge)

## Tools
These are tools that I use during development and infrastructure that I use for deployment.

![](https://img.shields.io/badge/Docker-blue?logo=docker&logoColor=white&style=for-the-badge)
![](https://img.shields.io/badge/Linux-gray?logo=linux&logoColor=white&style=for-the-badge)
![](https://img.shields.io/badge/Git-orange?logo=git&logoColor=white&style=for-the-badge)
![](https://img.shields.io/badge/MySQL-blue?logo=mysql&logoColor=white&style=for-the-badge)
![](https://img.shields.io/badge/JetBrains-blueviolet?logo=jetbrains&logoColor=white&style=for-the-badge)
![](https://img.shields.io/badge/Redis-red?logo=redis&logoColor=white&style=for-the-badge)
![](https://img.shields.io/badge/MongoDB-green?logo=mongodb&logoColor=white&style=for-the-badge)
![](https://img.shields.io/badge/Visual%20Studio%20Code-blue?logo=visual-studio-code&logoColor=white&style=for-the-badge)
![](https://img.shields.io/badge/Jupyter%20Notebooks-orange?logo=jupyter&logoColor=white&style=for-the-badge)
![](https://img.shields.io/badge/Data%20Version%20Control-blue?logo=dvc&logoColor=white&style=for-the-badge)
